(ns graalvm-cognitect-api-fix.protocols
  (:require
   [cognitect.aws.protocols.rest-xml]
   [cognitect.aws.protocols.query]
   [cognitect.aws.protocols.json]))
